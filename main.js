var paginas = 0;
var paginaAct = 1;
var numeroSeries = 0;
var Series;
var SeriesVotadas = new Array();
var numSeriesVotadas = 0;

$('.carousel').carousel(
	{
		interval: false
	}
);

function enviarVotacion()
{
	var finales = new Array();
	var contador = 0;
	for(var indice in SeriesVotadas)
	{
		if (SeriesVotadas[indice] != null) 
		{
			finales.push(SeriesVotadas[indice]);
			contador++;
		}
		if (contador == 30)
		{
			break;
		}
	}
	//console.log("el arreglo inal quedo: " + finales);
	var finalesStr = JSON.stringify(finales);
	var post = $.post( "votaciones.php", { elegidas: finalesStr}, function(data){
		alert(data);
	} );
	post.error(function()
		{alert("algo sucedio mal")});
	//var finalesObj = jQuery.parseJSON(finalesStr);
	//console.log(finalesStr);
	//console.log(finalesObj);
	//alert("su veredicto se ha almacenado con exito");
}
function asignaResumen()
{
	var resultadoTag = "No has votado por ninguna serie :(";
	var contador = 0;
		for(var indice in SeriesVotadas)
		{
			if (contador == 0) 
			{
				resultadoTag = "";
			}
			
			for (var i = 0; i < Series.length; i++) 
			{
				if (Series[i].id == indice)
				{
					resultadoTag += "<span>" + (contador+1) + "."+ Series[i].nombre + "</span><br>";
					break;
				}
			}
			contador++;
		}
		$('#contenidoResumen').html(resultadoTag);
		if (numSeriesVotadas == 30)
		{
			$('#contenidoResumen').append("<center><button onclick='enviarVotacion()' class='btn btn-primary'>Enviar votación</button></center>");
		}
}

$('#resumenSeries').on('change', function()
{
	if ($(this).is(':checked')) 
	{
		console.log('se presiono el resumen');
		asignaResumen();
	}
	
});

function OperacionExitosa(r)
{
	//var resultado = "el id de la serie es: ";
	//$("#contenedorSeries").append(resultado);
	Series = r;
	numeroSeries = r.length;
	var cantidadSeries = r.length;
	var contador = 0;
	var resultadoTag = "";
	paginas = Math.ceil(numeroSeries/12)
	console.log(paginas);
	$.each(r,function(i,item)
		{
			contador = i;	
			if (contador < 12)
			{
				if (i == 0 || i%4 == 0)
				{
					resultadoTag += "<br><div class='row'> ";
				}
				resultadoTag += "<div class='contenedorImagen col-xs-12 col-sm-6 col-md-3 text-truncate' id='" + r[i].id + "'> ";
				/*$("#contenedorSeries").append(r[i].id + "<br>");*/
				resultadoTag += "<center><span class='encabezadoSerie'><strong>"+ r[i].nombre + "</strong></span></center>";

				resultadoTag += "<div><center><img data-original='" + r[i].fotos[0].ruta + "' class = 'lazy img-responsive img-thumbnail'></center></div>";
				resultadoTag += "<center><span class='encabezadoSerie small'>"+ r[i].nombreParticipante + "</span></center>";
				resultadoTag += "<center><button onclick='asignaNuevaSerie(" + r[i].id + ")' class='btn btn-primary btn-inline'>Ver Serie</button>&nbsp&nbsp<div class='checkbox-inline'><label><input type='checkbox' onchange='asignarSerieVotada(" + r[i].id + ")' value='"+ r[i].id +"' ><span class='glyphicon glyphicon-thumbs-up' aria-hidden='true'></span></label></div></center></div>";
				
				if (((contador+1)%4 == 0 && contador !=0) || (cantidadSeries == (contador+1) && cantidadSeries < 4) || cantidadSeries == (contador+1)) 
				{
					resultadoTag += "</div>";
				}
			}
			else
			{
				return false;
			}
		}
	)
	$("#contenedorSeries").append(resultadoTag);
	$("#contenedorPaginacion").append("<br>");
	$("img.lazy").lazyload({
		threshold : 20
	});
	if (paginas == 1)
	{
		$('.glyphicon-arrow-right').css("visibility","hidden");
	}
	/*for (var i = 1; i <= paginas; i++)
	{
		$("#contenedorPaginacion").append("<button class='btn btn-primary btn-inline'>"+i+"</button>");
	}*/
}
function OperacionErronea(r)
{
	var resultado = "La operacion no tuvo existo";
	$("#contenedorSeries").append(resultado);
	console.log(r);
}
$(document).ready(
	function()
	{
		var post = $.post("series.php",OperacionExitosa,'json');
		post.error(OperacionErronea);
		//alert("Hola mundo, mi amigo Jquery");
		//$('a').on('click',function(e){ alert("hola"); });
		/*for(var n = 1; n<=4; n++)
		{
			//$(candenaAux)
			for(var j = 1; j<=4; j++)
			{
				var candenaAux = ".contenedorImagen div:nth-child(" + j + ")";
				if(n == j)
				{
					alert($(candenaAux).text());
				}
				else
				{
					$(candenaAux).hide("slow");
				}
			}
		}*/
	}
);

function asignaNuevaSerie (idSerie)
{
	var resultadoSlider = "";
	//alert("Es la serie: " + idSerie);
	$.each(Series,function(i,item)
		{
			if(Series[i].id == idSerie)
			{
				$.each(Series[i].fotos,function(j,item)
				{
					if (j == 0)
					{
						resultadoSlider += "<div class='item active'><img src='" + Series[i].fotos[j].ruta + "' alt='" + Series[i].fotos[j].titulo + "'>";
					}
					else
					{
						resultadoSlider += "<div class='item'><img src='" + Series[i].fotos[j].ruta + "' alt='" + Series[i].fotos[j].titulo + "'>";
					}
					resultadoSlider += "<div class='carousel-caption hidden-xs'>";
					resultadoSlider += "<h3>" +Series[i].nombre + "</h3>";
					resultadoSlider += "<p>" + Series[i].nombreParticipante+ "</p></div></div>";
					//resultadoSlider += "foto: " + Series[i].fotos[j].ruta + " ";
				})
				$('#contenedorSlides').html(resultadoSlider);
				$('.carousel-indicators').find('li').removeClass('active');
				$('.carousel-indicators').find('li').first().addClass('active');
				$('body, html').animate({scrollTop: '0px'}, 'slow');
				return false;
			}
		}
	)
	//alert(resultadoSlider);
}

function asignaNuevaPagina (valor)
{
	if (valor == 2) 
	{
		paginaAct -= 2;
	}
	var inicio = 12 * paginaAct;
	console.log("inicio: " + inicio);
	console.log("paginas: " + paginas);
	var resultadoTag = "";
	var contador = 0;
	for (var i = inicio; i < numeroSeries && contador < 12; i++) 
	{
		if (contador == 0 || contador%4 == 0)
		{
			resultadoTag += "<br><div class='row'> ";
		}
		resultadoTag += "<div class='contenedorImagen col-xs-12 col-sm-6 col-md-3 text-truncate' id='" + Series[(i)].id + "'> ";
		/*$("#contenedorSeries").append(r[i].id + "<br>");*/
		resultadoTag += "<center><span class='encabezadoSerie'><strong>"+ Series[(i)].nombre + "</strong></span></center>";

		resultadoTag += "<div><center><img data-original='" + Series[(i)].fotos[0].ruta + "' class = 'lazy img-responsive img-thumbnail'></center></div>";
		resultadoTag += "<center><span class='encabezadoSerie small'>"+ Series[i].nombreParticipante + "</span></center>";
		if (SeriesVotadas[Series[i].id] == null)
		{
			resultadoTag += "<center><button onclick='asignaNuevaSerie(" + Series[i].id + ")' class='btn btn-primary btn-inline'>Ver Serie</button>&nbsp&nbsp<div class='checkbox-inline'><label><input type='checkbox' onchange='asignarSerieVotada(" + Series[i].id + ")' value='"+ Series[i].id +"' ><span class='glyphicon glyphicon-thumbs-up' aria-hidden='true'></span></label></div></center></div>";
		}
		else
		{
			resultadoTag += "<center><button onclick='asignaNuevaSerie(" + Series[i].id + ")' class='btn btn-primary btn-inline'>Ver Serie</button>&nbsp&nbsp<div class='checkbox-inline'><label><input type='checkbox' onchange='asignarSerieVotada(" + Series[i].id + ")' value='"+ Series[i].id +"' checked><span class='glyphicon glyphicon-thumbs-up' aria-hidden='true'></span></label></div></center></div>";
		}
		if (((contador+1)%4 == 0 && contador !=0) || (numeroSeries == (inicio+contador+1))) 
		{
			resultadoTag += "</div>";
		}
		contador++;
	}
	//each
	$("#contenedorSeries").html(resultadoTag);
	$("#contenedorPaginacion").append("<br>");
	paginaAct +=1;
	
	console.log("paginas: " + paginas);
	console.log("paginaAct : " + paginaAct);
	$('#paginaActual').html(paginaAct);
	$('body, html').animate({scrollTop: '0px'}, 0);
	$("img.lazy").lazyload({
		threshold : 20
	}).removeClass("lazy");
	
	if (paginaAct == paginas) 
	{
		$('.glyphicon-arrow-right').css("visibility","hidden");
		$('.glyphicon-arrow-left').css("visibility","visible");
	}
	else
	{
		if (paginaAct > 1) 
		{
			$('.glyphicon-arrow-left').css("visibility","visible");
			$('.glyphicon-arrow-right').css("visibility","visible");
		}
		else
		{
			$('.glyphicon-arrow-left').css("visibility","hidden");
			$('.glyphicon-arrow-right').css("visibility","visible");
		}
	}
}

function asignarSerieVotada(idSerie)
{

	if (SeriesVotadas[idSerie] == null)
	{
		if (numSeriesVotadas < 30)
		{
		SeriesVotadas[idSerie] = idSerie;
		numSeriesVotadas+=1;
		//console.log("se añadio");
		}
		else
		{
			alert("Solo puedes votar por 30 fotos");
		}
	}
	else
	{
		delete SeriesVotadas[idSerie];
		numSeriesVotadas-=1;
		//console.log("se elimino");
	}
	/*
	console.log(SeriesVotadas);

	for(var indice in SeriesVotadas)
	{
		console.log('(indice)' + indice+':(valor)'+SeriesVotadas[indice]);
	}
	*/
	asignaResumen();
}