<?php
	require_once "config.php";
	/**
	* 
	*/

	class Conexion extends mysqli
	{

		public function __construct()
		{
			parent::__construct(DB_HOST, DB_USER,DB_PASS,DB_NAME);
			$this->query("SET NAMES 'utf8'");
			if ($this-> connect_errno)
			{
				die( "Fallo la conexión a MySQL: (" . $this-> mysqli_connect_errno(). ") " . $this-> mysqli_connect_error());
			}
			else
			{
				//echo "Conexión exitosa!";
			}
		}

	}

	$db = new Conexion();
?>