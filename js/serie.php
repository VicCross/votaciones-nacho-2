<?php
	/**
	*Developed by @VicCross
	*/
	require_once "foto.php";
	require_once "Seriecd.php";
	class serie
	{
		//campos
		public $id = "";
		public $nombreParticipante = "";
		public $nombre = "";
		public $votos = 0;
		public $fotos  = array();

		function __construct($id, $nombreParticipante, $nombre, $votos)
		{
			$this -> id = $id;
	 		$this -> nombre = $nombre;
	 		$this -> nombreParticipante = $nombreParticipante;
	 		$this-> votos = $votos;
			# code...
		}

		function agregarFoto ($nombre, $ruta)
		{
			$fotoAct = new Foto($nombre,$ruta);
			$this->fotos[] = $fotoAct;
		}

		public static function ObtenerSeries ()
		{
			return Seriecd::ObtenerSeries();
		}
	}
?>