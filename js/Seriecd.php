<?php 
	/**
	*Developed by @VicCross 
	*/
	require_once "conexion.php";
	require_once "serie.php";
	class Seriecd
	{
		public static function ObtenerSeries()
		{
			//$num = 0;
			$series = array();
			$conexionAct = new Conexion();
			if($sentencia = $conexionAct->prepare("SELECT id_participante, nombre,
				ap_paterno,ap_materno,serie,foto1,ruta1,foto2,ruta2,foto3,ruta3,foto4,ruta4,votos FROM soe2013_nacho_lopez.participantes"))
			{
				//echo "se pudo preparar";
				$sentencia ->bind_result($id,$nombre,$apPaterno,$apMaterno,$nombreSerie,$titulo1,$foto1,$titulo2,$foto2,$titulo3,$foto3,$titulo4,$foto4,$votos);
				$sentencia->execute();
				while ($sentencia->fetch())
				{
					$serieAct = new serie($id,$nombre. " ". $apPaterno." ".$apMaterno,$nombreSerie,$votos);
					$serieAct->agregarFoto($titulo1,str_replace("+","%20",str_replace("%2F","/","../".urlencode($foto1))));
					$serieAct->agregarFoto($titulo2,str_replace("+","%20",str_replace("%2F","/","../".urlencode($foto2))));
					$serieAct->agregarFoto($titulo3,str_replace("+","%20",str_replace("%2F","/","../".urlencode($foto3))));
					$serieAct->agregarFoto($titulo4,str_replace("+","%20",str_replace("%2F","/","../".urlencode($foto4))));
					$series[] = $serieAct;
				}
			}
			else
			{
				//echo "no se pudo preparar";
				die ("Mysql Error: " . $conexionAct->error);
			}
			return $series;
		}
	}

	//Seriecd::obtenerSeries();
?>