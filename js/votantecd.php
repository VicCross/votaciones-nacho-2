<?php
	/**
	*Developed by @VicCross 
	*/
	require_once "conexion.php";
	class Votantecd
	{
		public static function AlmacenarVotacion($usuario, $password, $votos)
		{
			$num = 0;
			$conexionAct = new Conexion();
			$resultado = array();
			$statusOp = false;

			for ($i=0; $i < 30; $i++)
			{
				if($sentencia = $conexionAct->prepare("UPDATE soe2013_nacho_lopez.participantes SET  votos = votos + 1 WHERE id_participante = ?"))
				{
					//echo "se pudo preparar";
					$sentencia ->bind_param("i",$votos[$i]);
					$sentencia->execute();
					$statusOp = true;//se guardo al menos 1 voto por alguna serie
				}
				else
				{
					//echo "no se pudo preparar";
					//die ("Mysql Error: " . $conexionAct->error);
				}
			}

			if ($statusOp == true) 
			{
				if($sentencia = $conexionAct->prepare("UPDATE soe2013_nacho_lopez.users SET  votacionRealizada = 1 WHERE usuario = ? and pass = ?"))
				{
					//echo "se pudo preparar";
					$sentencia ->bind_param("ss",$usuario, $password);
					$sentencia->execute();
				}
				else
				{
					//echo "no se pudo preparar";
					//die ("Mysql Error: " . $conexionAct->error);
				}
			}

			return $statusOp;
		}

		public static function VerificaVotante ($usuario, $password)
		{
			$flag = 0;
			$resultado = false;
			$conexionAct = new Conexion();
			if($sentencia = $conexionAct->prepare("SELECT votacionRealizada FROM soe2013_nacho_lopez.users WHERE  usuario = ? and pass = ?"))
			{
				//echo "se pudo preparar";
				$sentencia ->bind_param("ss",$usuario, $password);
				$sentencia ->bind_result($votacionRealizada);
				$sentencia->execute();
				while ($sentencia->fetch())
				{
					if ($votacionRealizada == 0)
					{
						$flag = 1;
					}
					else
					{
						$flag = 0;
					}
				}
			}
			else
			{
				//echo "no se pudo preparar";
				die ("Mysql Error: " . $conexionAct->error);
			}

			if ($flag == 1) 
			{
				$resultado = true;
			}

			return $resultado;
		}
	}
?>