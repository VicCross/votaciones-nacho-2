<?php 
	/**
	*Developed by @VicCross 
	*/
	require_once "votante.php";
	session_start();
	$seriesVotadas = json_decode($_POST['elegidas']);
	if(count($seriesVotadas) == 30)
	{
		if (isset($_SESSION["usuario"]) && isset($_SESSION["pass"]))
		{
			$votanteAct = new Votante($_SESSION["usuario"],$_SESSION["pass"],$seriesVotadas);
			//echo ($votanteAct->user);
			if ($votanteAct->verficaUsuario())
			{
				if ($votanteAct->guardarVotacion())
				{
					echo "tu votación se realizo con exito";
				}
				else
				{
					echo "tu votación no se ha podido almacenar";
				}
			}
			else
			{
				echo "el usuario no esta creado o ya ha votado";
			}
		}
		else
		{
			echo "usuario o contraseña no validos";
		}
	}
	else
	{
		echo "algo ha salido mal, intentalo nuevamente mas tarde :(";
	}
?>