<!--Developed by @Vic_Cross-->
<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
     <meta name="theme-color" content="#FEDB00">
	<title>Concurso Nacho Lopéz</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	<link href="css/index.css" rel="stylesheet">
</head>
<body>
	<div class="jumbotron">
		<center><h1>Concurso Nacho Lopéz</h1></center>
		<center><h2>#HomenajeNachoLópez</h2><center>
	</div>
	<div class="container">
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
	    	<!-- Wrapper for slides -->
	    	<div class="carousel-inner" role="listbox" id="contenedorSlides">
	    		<div class="item active">
	    			<img src="../img_concurso/Miguel%20%C3%81ngelFloresAguilera/1.Miguel_Flores_01.JPG" alt="1.Miguel_Flores_01.JPG">
	    		</div>
	    		
	    		<div class="item">
	      			<img src="../img_concurso/Miguel%20%C3%81ngelFloresAguilera/2.Miguel_Flores_02.JPG" alt="2.Miguel_Flores_02.JPG">
	    		</div>

	    		<div class="item">
	      			<img src="../img_concurso/Miguel%20%C3%81ngelFloresAguilera/3.Miguel_Flores_03.jpg" alt="3.Miguel_Flores_03.jpg">
	    		</div>

	    		<div class="item">
	      			<img src="../img_concurso/Miguel%20%C3%81ngelFloresAguilera/4.Miguel_Flores_04.jpg" alt="4.Miguel_Flores_04.jpg">
	    		</div>
	    	</div>

	    	<!-- controles izquierda y derecha -->
	    	<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
	    		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	    		<span class="sr-only">Anterior</span>
	  		</a>
	  		<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
	    		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	    		<span class="sr-only">Siguiente</span>
	  		</a>
		</div>
	</div>
	<div class="seleccion">
		<div id="tituloSeleccion">
			<span id="tituloSeleccion">Selecciona tus series favoritas</span>
		</div>
		<br>
		<div id="contenedorSeries" class="container">
			
		</div>
		<br>
		<br>
		<div id="contenedorPags" class="container">
			<span class='glyphicon glyphicon-arrow-right' aria-hidden='true' onclick='asignaNuevaPagina(1)'></span>
			<span class='glyphicon glyphicon-arrow-left' aria-hidden='true' onclick='asignaNuevaPagina(2)'></span>
			<div id="paginaActual">1</div>
		</div>
	</div>
	<div id="contenedorElegidas" class="container">
			<div class="resumen">
				<input id="resumenSeries" type="checkbox"/>
				<label data-expanded="Series por las que he votado" data-collapsed="Series por las que he votado"></label>
				<div id="contenidoResumen" class="resumen-content">
				
				</div>
			</div>
	</div>
	<div id="contenedorUsuario">
		<center>
			<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
			<?php
				if(isset($_SESSION["usuario"]) && isset($_SESSION["pass"]))
				{
				 echo " ".$_SESSION["usuario"];
			     echo "<br><a href='../concursovotaciones/cerrarsesion.php'>Cerrar Sesión</a>";
				}else
				{
					echo "<meta http-equiv='refresh' content='0;url=../concursovotaciones/index.php'>";
				}
			    echo "<br>";
	    	?>
	    	<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
	    	<br>
	    	<br>
		</center>
	</div>
	<script src="js/jquery-2.2.4.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery_lazyload-1.9.7/jquery.lazyload.js"></script>
	<script src="js/main.js"></script>
</body>
</html>