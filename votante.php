<?php
	/**
	*Developed by @VicCross 
	*/
	require_once "votantecd.php";
	class Votante
	{
		public $user = "";
		public $password = "";
		public $votos  = array();

		function __construct($user, $password, $votos)
		{
			$this -> user = $user;
	 		$this -> password = $password;
	 		$this-> votos = $votos;
			# code...
		}

		function guardarVotacion()
		{
			if (Votantecd::AlmacenarVotacion($this -> user, $this -> password, $this-> votos))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		function verficaUsuario()
		{
			if (Votantecd::VerificaVotante($this -> user, $this -> password))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
?>